FROM docker-registry.wikimedia.org/php7.2-fpm

USER root
RUN apt update && apt install -y --no-install-recommends git ca-certificates

RUN mkdir -p /var/www && chown www-data: /var/www /srv

COPY /mv-mw-image-builder/build-image /

USER www-data
RUN /build-image /srv/mediawiki

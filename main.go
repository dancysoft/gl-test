package main

import (
    "fmt"
    "net/http"
)

func main() {
    http.HandleFunc("/", HelloServer)
    http.ListenAndServe(":8080", nil)
}

func HelloServer(w http.ResponseWriter, r *http.Request) {
    path := r.URL.Path[1:]
    if path == "" {
       path = "NoName"
    }
    fmt.Fprintf(w, "Hello, %s!\n", path)
}
